﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    private CharacterController character;

    public Transform bulletSpawn;

    public Bullet bulletPrefab;

    public float speed;
    public float rotationSpeed;

    public int score;

    // Use this for initialization
    void Start () {
        character = GetComponent<CharacterController>();
	}

    public void Death() {
        Application.LoadLevel(0);
    }

    // Update is called once per frame
    void Update () {

        float f = Input.GetAxis("Vertical");

        float r = Input.GetAxis("Horizontal");
        
        character.SimpleMove(transform.forward * f * speed);

        transform.Rotate(0, rotationSpeed * r * Time.deltaTime, 0);

        if(Input.GetButtonDown("Fire1")) {
            Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        }
	
	}
}
