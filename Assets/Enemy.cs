﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public int damage;

    private AudioSource audioS;

    private Player player;

    private Rigidbody body;

    public float accelleration;

	// Use this for initialization
	void Start () {
        player = FindObjectOfType<Player>();
        body = GetComponent<Rigidbody>();
        audioS = GetComponent<AudioSource>();

    }

    public void Death() {
        player.score++;
        audioS.Play();

        Destroy(transform.GetChild(0).gameObject);

        Destroy(gameObject,3);
    }
	
	// Update is called once per frame
	void Update () {

        Vector3 p = (-transform.position + player.transform.position).normalized;

        body.AddForce(p * accelleration * Time.deltaTime);
	
	}


    void OnCollisionEnter(Collision collision) {
        Health h = collision.collider.GetComponentInParent<Health>();

        if (h) {
            h.TakeDamage(damage);
        }   

    }
}
