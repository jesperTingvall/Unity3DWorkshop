﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

    public int life;

    public void TakeDamage(int d) {

        life -= d;

        if (life<=0) {
            SendMessage("Death");
            Destroy(this);
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
