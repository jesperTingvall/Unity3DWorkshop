﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public float speed;

    public float lifetime;

    private Rigidbody body;

    public int damage;

	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody>();
        body.velocity = transform.forward * speed;
        Destroy(gameObject, lifetime);
	}

    void OnCollisionEnter(Collision collision) {
        Health h = collision.collider.GetComponentInParent<Health>();

        if (h) {
            h.TakeDamage(damage);

        }
        Destroy(gameObject);

    }

    // Update is called once per frame
    void Update () {
	
	}
}
